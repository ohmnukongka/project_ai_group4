from dash import Dash, dash, Input, Output, State
from dash.dependencies import Input, Output, ALL, State, MATCH, ALLSMALLER
from pycaret.classification import *
from dash.dependencies import Input, Output
import dash_bootstrap_components as dbc
import plotly.express as px
import pandas as pd
import numpy as np
import dash_core_components as dcc
import dash_html_components as html


app = Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP])

df = pd.read_excel("data_dropout_59-64.xlsx")
df["FUND_NAME_CODE"] = df["FUND_NAME_CODE"].replace(["N","Y","other"],[1,2,3])
model = load_model("group4-model")
last_n_click_botton = 0

bardata1 = df[df["STUDY_STATUS"]=="G"]
bardata2 = df[df["STUDY_STATUS"]=="R"]
bardata = pd.concat([bardata1, bardata2])
bardata.rename(columns={"SEX_NAME_THAI":"เพศ"},inplace=True)
# Define the app


# Create the histogram graph using Plotly
hist = px.histogram(
      bardata, x = "STUDY_STATUS",nbins = 5 ,barmode = "group",barnorm = 'percent',
      color = "เพศ",color_discrete_map = {'ชาย':'#7ee7e8','หญิง':'#18898a'}
                    ).update_layout(
      {'plot_bgcolor':'#2b4356','paper_bgcolor':'#314c61'},font_color = "#d0dde7",
      font_size = 20,xaxis_title = 'สถานะการศึกษา',yaxis_title = 'จำนวน'
      )

#line graph
linedata1 = df[df["STUDY_STATUS"]=="G"].groupby("ADMIT_YEAR").size()
linedata2 = df[df["STUDY_STATUS"]=="R"].groupby("ADMIT_YEAR").size()
# define data
x_data = linedata1.index
y_data1 = linedata1.values
y_data2 = linedata2.values

# create line graph with two lines
line_graph = dcc.Graph(
    id = 'line-graph',
    figure = {
        'data': [
            {
                'x': x_data,
                'y': y_data1,
                'type': 'line',
                'name': 'ผ่านการศึกษา',
                'line': {'color': '#3fdcdd'}
            },
            {
                'x': x_data,
                'y': y_data2,
                'type': 'line',
                'name': 'ตกออก',
                'line': {'color': '#178283'}
            }
        ],
        'layout': {
            'plot_bgcolor': '#d8f8f8',
            'paper_bgcolor': '#3f617c',
            'font' : {
                'color' : '#d0dde7',
                 'size': 22
            },
            'xaxis': {
                'title': 'ปี'
            },
            'yaxis': {
                'title': 'จำนวน'
            }
        }
    },style = {'height': '337px', 'width': '700px'},
)

fig = px.pie(
      values = [50, 50],
      names = ["ผ่านการศึกษา", "ตกออก"],
      color = ["ผ่านการศึกษา", "ตกออก"],
      color_discrete_map = {'ผ่านการศึกษา':'#25d5d7',
                            'ตกออก':'#a8efef',
                        }).update_layout(
      {"plot_bgcolor": '#CED6DB', "paper_bgcolor": '#d8f8f8'},
      font = dict(size = 20)
            )

carousel = dbc.Carousel(
    items = [
        {
            "key": "1",
            "src": "/static/images/1.png",
        },
        {
            "key": "2",
            "src": "/static/images/2.png",
        },
        {
            "key": "3",
            "src": "/static/images/3.png",
        },
    ],
    interval = 3000,
    variant = "dark",
)
text_input = html.Div(
    [
        html.Br(),
            dcc.Input(id="input1_1",value=None, placeholder="เกรดปี 1 เทอม 1", type="number",min=0, max=4,step=0.01,
                    style={'width': "130px",
                           "font-size":"15px",
                           'border-radius':'5px',
                           'margin-left':'170px',
                           "text-align":"center"}),
            dcc.Input(id="input1_2",value=None, placeholder="เกรดปี 1 เทอม 2", type="number",min=0, max=4,step=0.01,
                    style={'width': "130px",
                           "font-size":"15px",
                           'border-radius':'5px',
                           'margin-top':'10px',
                           'margin-left':'20px',
                            'margin-right':'20px',
                            "text-align":"center"}),
            dcc.Input(id="input1_3",value=None, placeholder="เกรดปี 1 เทอม 3", type="number",min=0, max=4,step=0.01,
                    style={'width': "130px",
                           "font-size":"15px",
                           'border-radius':'5px',
                           'margin-top':'10px',
                           "text-align":"center"}),


            dcc.Input(id="input2_1",value=None, placeholder="เกรดปี 2 เทอม 1", type="number",min=0, max=4,step=0.01,
                    style={'width': "130px",
                           "font-size":"15px",
                           'border-radius':'5px',
                           'margin-left':'800px',
                           'margin-left':'50px',
                            "text-align":"center"}),
            dcc.Input(id="input2_2",value=None, placeholder="เกรดปี 2 เทอม 2", type="number",min=0, max=4,step=0.01,
                    style={'width': "130px",
                           "font-size":"15px",
                           'border-radius':'5px',
                           'margin-top':'10px',
                           'margin-left':'20px',
                            'margin-right':'20px',
                            "text-align":"center"}),
            dcc.Input(id="input2_3",value=None, placeholder="เกรดปี 2 เทอม 3", type="number",min=0, max=4,step=0.01,
                    style={'width': "130px",
                           "font-size":"15px",
                           'border-radius':'5px',
                           'margin-top':'10px',
                           "text-align":"center"}),
        
    
        html.Br(),
            dcc.Input(id="input3_1",value=None, placeholder="เกรดปี 3 เทอม 1", type="number",min=0, max=4,step=0.01,
                    style={'width': "130px",
                           "font-size":"15px",
                           'border-radius':'5px',
                           'margin-left':'170px',
                           "text-align":"center"}),
            dcc.Input(id="input3_2",value=None, placeholder="เกรดปี 3 เทอม 2", type="number",min=0, max=4,step=0.01,
                    style={'width': "130px",
                           "font-size":"15px",
                           'border-radius':'5px',
                           'margin-left':'20px',
                            'margin-right':'20px',
                            "text-align":"center"}),
            dcc.Input(id="input3_3",value=None, placeholder="เกรดปี 3 เทอม 3", type="number",min=0, max=4,step=0.01,
                    style={'width': "130px",
                           "font-size":"15px",
                           'border-radius':'5px',
                           "text-align":"center"}),


            dcc.Input(id="input4_1",value=None, placeholder="เกรดปี 4 เทอม 1", type="number",min=0, max=4,step=0.01,
                    style={'width': "130px",
                           "font-size":"15px",
                           'border-radius':'5px',
                           'margin-left':'50px',
                           'margin-top':'50px',
                           "text-align":"center"}),
            dcc.Input(id="input4_2",value=None, placeholder="เกรดปี 4 เทอม 2", type="number",min=0, max=4,step=0.01,
                    style={'width': "130px",
                           "font-size":"15px",
                           'border-radius':'5px',
                           'margin-left':'20px',
                            'margin-right':'20px',
                           "text-align":"center"}),
            dcc.Input(id="input4_3",value=None, placeholder="เกรดปี 4 เทอม 3", type="number",min=0, max=4,step=0.01,
                    style={'width': "130px",
                           "font-size":"15px",
                           'border-radius':'5px',
                           "text-align":"center" }),

        html.Br(),
            dcc.Input(id="input_GPA",value=None, placeholder="GPA", type="number",min=0, max=4,step=0.01,
                    style={'width': "130px",
                           "font-size":"15px",
                           'border-radius':'5px',
                           'margin-top':'50px',
                           'margin-left':'570px',
                           "text-align":"center"}), 
        
    ]
)

@app.callback(
            Output("output", "figure"),
            Input("input1_1", "value"),
            Input("input1_2", "value"),
            Input("input1_3", "value"),
            Input("input2_1", "value"),
            Input("input2_2", "value"),
            Input("input2_3", "value"),
            Input("input3_1", "value"),
            Input("input3_2", "value"),
            Input("input3_3", "value"),
            Input("input4_1", "value"),
            Input("input4_2", "value"),
            Input("input4_3", "value"),
            Input("input_GPA", "value"),
            Input("input_major", "value"),
            Input("input_fund", "value"),
            Input("input_parent", "value"),
            Input("input_province", "value"),
            Input("button_pre","n_clicks"),
            )
            

def output_text(value1_1,value1_2,value1_3,value2_1,value2_2,value2_3,value3_1,value3_2,value3_3,
                value4_1,value4_2,value4_3,value_GPA,value_major,value_fund,value_parent,value_province,n_click_botton):
        global last_n_click_botton
        global fig
        if n_click_botton != last_n_click_botton:
              last_n_click_botton = n_click_botton
              dicts_input = {
                    "MAJOR_ID":[value_major],
                    "เกรดปี1เทอม1":[value1_1],
                    "เกรดปี1เทอม2":[value1_2],
                    "เกรดปี1เทอม3":[value1_3],
                    "เกรดปี2เทอม1":[value2_1],
                    "เกรดปี2เทอม2":[value2_2],
                    "เกรดปี2เทอม3":[value2_3],
                    "เกรดปี3เทอม1":[value3_1],
                    "เกรดปี3เทอม2":[value3_2],
                    "เกรดปี3เทอม3":[value3_3],
                    "เกรดปี4เทอม1":[value4_1],
                    "เกรดปี4เทอม2":[value4_2],
                    "เกรดปี4เทอม3":[value4_3],
                    "GPA_SCHOOL":[value_GPA],
                    "PARENTS_MARRIED_NAME_CODE":[value_parent],
                    "FUND_NAME_CODE":[value_fund],
                    "INSTITUTION_PROVINCE_ID":[value_province],
                    }
              df_input = pd.DataFrame(dicts_input)
              df_input = df_input.fillna(0)
              predict_percent = model.predict_proba(df_input).max()
              var_from_input = model.predict(df_input)[0]
              print(var_from_input)
              d = {0:"G",1:"R"}
              variable2 = d[var_from_input]
              dict_var = {"R":"ตกออก","G":"ผ่านการศึกษา"}
              dd = {"R":"G","G":"R"}
              fig = px.pie(
                    values=[predict_percent,1-predict_percent],
                    names=[dict_var[dd[variable2]],dict_var[variable2]],
                    color=["Graduate","Retire"],
                    color_discrete_map={'Graduate':'#25d5d7',
                            'Retire':'#a8efef',
                        }).update_layout({"plot_bgcolor": '#CED6DB', "paper_bgcolor": '#d8f8f8'},font=dict(size=20))
              return fig
        return fig


app.layout = html.Div(
    style={
        'background-color': '#7fa2be',
        'background-size': 'cover',
        'background-position': 'center',
    },
    children = html.Div([
    dbc.NavbarSimple(
        children=[
        dbc.NavItem(dbc.NavLink("MainPage", href="http://127.0.0.1:8050/")),
    ],
    brand="GROUP 4",
    brand_href="#",
    color="#4c7697",
    dark=True,
    ),
    dbc.Container(
        [
            html.H1("Grade percent from predict", style={'font-size': 70, 'textAlign': 'center', 'color': '#194569'}),
            html.Hr(),
            dbc.Row(
                [
                    dbc.Col(
                        dbc.Card(
                            dbc.CardBody(
                                [   html.H1("การคาดการณ์สถานะการศึกษา", className="card-title 2", style= {"text-align":"center", 'color':'#d0dde7'}),
                                    html.Div(
                                        dcc.Graph(
                                            figure=px.pie(
                                                values=[50, 50],
                                                names=["Graduate", "Retire"],
                                            ),
                                            style={'height': '500px'},
                                            id = "output",
                                        ),
                                    ),
                                    html.H1("กรุณากรอกข้อมูล", className="card-title 2", 
                                            style= {"text-align":"center", 'color':'#d0dde7',"margin-top":"15px","font-size":"30px"}),
                                    text_input,
                                    html.Div([
                                    html.H1("สาขาวิชา", style={'font-size':'25px','margin-left': "300px",'margin-top': "18px","text-align":"center",'color':'#d0dde7'},),
                                    dbc.Select(
                                        id="input_major",
                                        options= [
                                            {'label': s, 'value': s} for s in np.sort(df['MAJOR_ID'].unique())
                                        ],
                                        style={"maxWidth": "110px",
                                               'margin-left': "300px"},
                                        placeholder="สาขาวิชา"
                                    )],style = {'display': 'inline-block'}),
                                    html.Div([
                                    html.H1("ทุนการศึกษา", style={'font-size':'25px', 'margin-left': '20px', "text-align":"center",'color':'#d0dde7'}),
                                    dbc.Select(
                                        id="input_fund",
                                        options=[
                                            {'label': s, 'value': s} for s in np.sort(df['FUND_NAME_CODE'].unique())
                                        ],
                                        style={"maxWidth": "135px",
                                               'margin-left': '30px'},
                                        placeholder="ทุนการศึกษา"
                                    )],style={'display': 'inline-block'}),
                                    html.Div([
                                    html.H1("สถานะทางครอบครัว", style={'font-size':'25px','margin-left': '20px',"text-align":"center",'color':'#d0dde7'}),
                                    dbc.Select(
                                        id="input_parent",
                                        options=[
                                            {'label': s, 'value': s} for s in np.sort(df['PARENTS_MARRIED_NAME_CODE'].unique())
                                        ],
                                        style={"maxWidth": "185px",
                                               'margin-left': '30px'},
                                        placeholder="สถานะทางครอบครัว"
                                    )],style={'display': 'inline-block'}),
                                    html.Div([
                                    html.H1("จังหวัด", style={'font-size':'25px','margin-left': '20px',"text-align":"center",'color':'#d0dde7'}),
                                    dbc.Select(
                                        id="input_province",
                                        options=[
                                            {'label': s, 'value': s} for s in np.sort(df['INSTITUTION_PROVINCE_ID'].unique())
                                        ],
                                        style={"maxWidth": "100px",
                                               'margin-left': '30px'},
                                        placeholder="จังหวัด"
                                    )],style={'display': 'inline-block'}),
                                    html.Br(),
                                    dbc.Button("Prediction", id = "button_pre", color="primary",n_clicks=0, 
                                               style={'margin-top': '20px',
                                                      'margin-left': "585px",
                                                      'color':'#a8efef'}),
                                ]
                            ),color = '#2b4356'
                        ),
                        width=12,
                    ),
                     
                ]
            ),
            dbc.Row(
                [
                    dbc.Col(
                        dbc.Card(
                            dbc.CardBody(html.Div(children=[
                                html.H1("กราฟแสดงอัตราสถานะการศึกษาของแต่ละเพศ", 
                                        className="card-title 2", 
                                        style= {"text-align":"center", 'color':'#d0dde7'}
                                        ),
                                        dcc.Graph(
                                            id='histogram-graph',
                                            figure=hist)
                                                            ]
                                                )
                                        ),color='#314c61'
                        ),
                        width=12,
                        style={"margin-top":"20px"},
                    ),
                    
                    dbc.Col(
                        dbc.Card(
                            dbc.CardBody(
                                html.Div(children=[
                                    html.H1("กราฟแสดงสถานะการศึกษาในแต่ละปี", 
                                            className="card-title 2", 
                                            style= {"text-align":"center", 'color':'#d0dde7'}),
                                            line_graph
                                            ]
                                        )
                                    ),color='#3f617c'
                        ),
                        width=7,
                        style={"margin-top":"20px"},
                    ),
                    dbc.Col(
                        dbc.Card(   
                                dbc.Row(
                                    dbc.Col(
                                        carousel,
                                        width={"size": 18, "offset": 0}
                                            )
                                        )    
                                ),
                        width=5,
                        style={"margin-top":"20px"},
                    ),
                ]
            ),
        ],
        className="mt-4",
        )
    ]
    )
)

if __name__ == '__main__':
    app.run_server(debug = True)